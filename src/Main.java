import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		String json = "[{\"address\":{\"city\":\"Tel Aviv\",\"name\":\"Florentin\",\"number\":25},\"age\":27,\"id\":123,\"name\":\"AdamEmployee\",\"smoke\":false},{\"address\":{\"city\":\"Holon\",\"name\":\"Itzahc Rabyin\",\"number\":69},\"age\":24,\"id\":122,\"name\":\"omriManager\",\"smoke\":false}]";
		FileReader fin = new FileReader(new File("json.json"));
		Gson g = new Gson();
		Person[] p = g.fromJson(fin, Person[].class);
		System.out.println(p[0].toString());
		System.out.println(p[1].toString());
	}

}
